# Publizjr2

Publizjr makes web pages. PHP and file based. No database. Fully aware of localization and translations. Each page is a folder. Each page can be extended and styled separately or similarly depending on your needs. 

Publizjr is a FOSS product by OmegaJunior Consultancy. Contact: publizjr@protonmail.com, or open an issue right here on Codeberg.  

  

## You want to concentrate on your content.
*You let a PHP programmer and a Front-End Developer deal with the technicalities. These people can be you, at a different time. But while you are writing your article, the publication system should not distract.*

Publizjr lets you deal with your articles. You write your texts, and provide only the necessary mark-up using BB Coding. Then you upload, and boom! Your new web page is on-line.  

  

## You define who else can edit your article.
*Sometimes you collaborate with other editors to create your article. Sometimes you don’t. You require a system that allows granularity of access permisssions.*

Each Publizjr webpage lives in its own folder on the file system of your site’s web server, Therefore, the webmaster can grant access to you for your pages, and to other editors for their pages.  

  

## Publication should be immediate, and from anywhere.
*Whether you use an iPad or any other tablet, an Android / Linux / Mac / Windows / Unix computer, or even any smartphone: you want access to your articles, and if you publish an article, it should show up instantly.*

Publizjr articles can be accessed via FTP and any other fileserver protocol your web hosting provider offers. No application that only works on Windows, no impressive but aggravating caching construction, no additional layer between you and your content… except your favourite file upload tool or app.  

  

## Sometimes you want to test your article changes.
*You want to have your article reviewed or revised before it goes live. Or you need to test how the text flows around the pictures and other media files. And you want to do that without it affecting your existing article.*

Publizjr deals with each page individually. Thus, an error in one article doesn’t influence any others. Each page lives in its own folder. Thus, to test your article, you can create a copy folder. As long as you don’t publish a link to the test page, the general public will be none the wiser.  

  

## Unlimited number of pages, editors, servers.
*Free web page builders that offer only 1 page, that force you to pay extra for more pages? Publication systems that allow only 5 editors at a time? Buying extra licenses to run multiple servers?*

None of that applies to Publizjr. You are welcome to create as many pages as you need, with as many editors as you like, on as many servers as required. Limitations in this realm are set by your web hosting provider: Publizjr offers freedom. We consider it Open Source Software. We make money by getting paid for implementations. 

  

## Near-Infinite Extensibility
*You have an idea about what you want right now, what should go into your web pages, and how it should work. And you know that a year ago, your thoughts differed. The publication system should allow for such changes, preferably without breaking your whole site.*

Publizjr is built using PHP. That means that any PHP programmer can change the contents of the Generator Template (which converts your article into a webpage). Parts can be moved, added, deleted, changed… and this can be done live, in real time, without breaking your existing webpages.

The Generator Template is just a text file in the web site, meaning it can be copied and placed under a different name, outside the public eye, allowing for testing changes without anyone seeing it, but those in the know.

We created instructions for adding the following functionality:

* Adding Audio
* Adding Language Awareness for multi-lingual content
* Adding Extensibility
* Integration of Social Sharing via AddThis
* Integration of Google Analytics
* Integration of Page Comments via Disqus
* Integration of Web Forms via JotForm

Any capable PHP programmer can add these functions, provided they have the necessary access.

If you feel the need to add more, let us know! Contact publizjr@protonmail.com or open an issue in the issue tracker right here on Codeberg.

